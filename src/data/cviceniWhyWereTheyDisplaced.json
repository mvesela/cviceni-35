{
  "cviceni": {
    "version": {
      "global": "<%= version %>",
      "content": 2
    },
    "language": "en",
    "katalog": [
      "public",
      "test"
    ],
    "id": 49,
    "slug": "why-were-they-displaced",
    "nazev": "Why Were They Displaced?",
    "autor": [
      "Kamil Činátl",
      "Matěj Spurný",
      "Tereza Vávrová"
    ],
    "klicovaSlova": [
      "The Munich Agreement",
      "World War II",
      "displacement",
      "cause and consequences",
      "changing perspectives"
    ],
    "anotace": {
      "ucitel": "This activity offers a pair of photographs that capture the displacement of Czechs from the border area in 1938 and the displacement of the Germans from Czechoslovakia in 1945. Students get a sense of the historical events from the perspective of concrete people. They use other sources (the Munich Agreement and the presidential decree modifying the citizenship of Germans and Hungarians) in order to grasp the various reasons and circumstances surrounding the displacements.",
      "verejna": "Try to empathize with a boy and a woman who are forced to leave their homes at the time of the Second World War. How could this happen? Use the important documents to help you and find out how these events forever influenced the lives of thousands."
    },
    "trvani": 45,
    "funkce": [
      "lupa",
      "text",
      "svg",
      "znacky"
    ],
    "pdf": {
      "doporucenyPostup": "why-were-they-displaced.pdf"
    },
    "uvodniObrazek": "why-were-they-displaced/pic-01-1280w.jpg"
  },
  "slajd": [
    {
      "zadani": {
        "hlavni": "Look at the photograph.",
        "rozsirujici": ""
      },
      "napoveda": {
        "aktivity": [
          "lupa"
        ],
        "text": "When you left click on the image, you can magnify sections of it."
      },
      "prameny": [
        "why-were-they-displaced/pic-00-1024h.jpg"
      ],
      "popisky": [
        "Czech citizens leaving the borderlands, which were seized by Nazi Germany after the Munich Agreement (Polička, October 1938). Source: ČTK"
      ]
    },
    {
      "zadani": {
        "hlavni": "Look at the other photograph.",
        "rozsirujici": ""
      },
      "napoveda": {
        "aktivity": [
          "lupa"
        ],
        "text": "When you left click on the image, you can magnify sections of it."
      },
      "prameny": [
        "why-were-they-displaced/pic-01-1280w.jpg"
      ],
      "popisky": [
        "Czech Germans are awaiting the transport for the displacement from Czechoslovakia (Prague Holešovice, Strossmayerovo náměstí, May 1945). Source: ČTK."
      ]
    },
    {
      "zadani": {
        "hlavni": "Describe what is happening in the photograph.",
        "rozsirujici": ""
      },
      "napoveda": {
        "aktivity": [
          "svg"
        ],
        "text": "You create marks by left clicking on the photo with the mouse. When you right click, you open a menu offering the following operations: change the color of the mark, (T) write a comment, (X) delete the mark."
      },
      "svg": {
        "soubory": [
          {
            "id": "svg-1",
            "soubor": "why-were-they-displaced/pic-00-1024h.jpg",
            "funkce": [
              "text",
              "znacky"
            ]
          }
        ]
      }
    },
    {
      "zadani": {
        "hlavni": "Describe what is happening in the photograph.",
        "rozsirujici": ""
      },
      "napoveda": {
        "aktivity": [
          "svg"
        ],
        "text": "You create marks by left clicking on the photo with the mouse. When you right click, you open a menu offering the following operations: change the color of the mark, (T) write a comment, (X) delete the mark."
      },
      "svg": {
        "soubory": [
          {
            "id": "svg-2",
            "soubor": "why-were-they-displaced/pic-01-1280w.jpg",
            "funkce": [
              "text",
              "znacky"
            ]
          }
        ]
      }
    },
    {
      "zadani": {
        "hlavni": "Try to empathize with the boy and the woman in the photographs.",
        "rozsirujici": "Describe how they may have felt about the situation and what they might have said."
      },
      "napoveda": {
        "aktivity": [
          "svg"
        ],
        "text": "You create marks by left clicking on the photo with the mouse. When you right click, you open a menu offering the following operations: change the color of the mark, (T) write a comment, (X) delete the mark."
      },
      "svg": {
        "soubory": [
          {
            "id": "svg-3",
            "soubor": "why-were-they-displaced/pic-00-1024h.jpg",
            "funkce": [
              "text",
              "znacky",
              "komiks"
            ],
            "komiks": [
              {
                "kategorie": "text",
                "subkategorie": "promluva",
                "pozice": [
                  140,
                  360
                ],
                "subjekt": [
                  "top",
                  "right"
                ]
              },
              {
                "kategorie": "text",
                "subkategorie": "myslenka",
                "pozice": [
                  480,
                  180
                ],
                "subjekt": [
                  "bottom",
                  "left"
                ]
              }
            ]
          },
          {
            "id": "svg-4",
            "soubor": "why-were-they-displaced/pic-01-1024w.jpg",
            "funkce": [
              "text",
              "znacky",
              "komiks"
            ],
            "komiks": [
              {
                "kategorie": "text",
                "subkategorie": "promluva",
                "pozice": [
                  375,
                  350
                ],
                "subjekt": [
                  "top",
                  "right"
                ]
              },
              {
                "kategorie": "text",
                "subkategorie": "myslenka",
                "pozice": [
                  705,
                  175
                ],
                "subjekt": [
                  "bottom",
                  "left"
                ]
              }
            ]
          }
        ]
      }
    },
    {
      "zadani": {
        "hlavni": "Read the excerpts from historical documents and search for the causes of displacement.",
        "rozsirujici": "Mark causes of events."
      },
      "napoveda": {
        "aktivity": [
          "text-editor"
        ],
        "text": "With the left mouse button, highlight the chosen word or phrase. This offers a list from which you can choose the category you’re marking. You can erase the highlight using the X."
      },
      "textovyEditor": {
        "texty": [
          {
            "id": 0,
            "text": "text-00",
            "funkce": "zvyraznovani",
            "menu": [
              {
                "title": "Mark the causes",
                "classes": "text-orange",
                "command": "toggle-class",
                "commandName": "mnichovska-dohoda",
                "commandOptions": {
                  "toggleClass": "node-selected node-text-orange",
                  "closeBtn": true,
                  "comments": false,
                  "commentsClass": "text-orange"
                }
              }
            ]
          },
          {
            "id": 1,
            "text": "text-01",
            "funkce": "zvyraznovani",
            "menu": [
              {
                "title": "Mark the causes",
                "classes": "text-red",
                "command": "toggle-class",
                "commandName": "ustavni-dekret",
                "commandOptions": {
                  "toggleClass": "node-selected node-text-red",
                  "closeBtn": true,
                  "comments": false,
                  "commentsClass": "text-red"
                }
              }
            ]
          }
        ]
      }
    },
    {
      "zadani": {
        "hlavni": "Read the text and put the photographs in historical context.",
        "rozsirujici": "Mark important passages."
      },
      "napoveda": {
        "aktivity": [
          "text-editor"
        ],
        "text": "With the left mouse button, highlight the chosen word or phrase. This offers a list from which you can choose the category you’re marking. You can erase the highlight using the X."
      },
      "textovyEditor": {
        "texty": [
          {
            "id": 2,
            "text": "text-02",
            "funkce": "zvyraznovani",
            "menu": [
              {
                "title": "Mark the passage",
                "classes": "text-orange",
                "command": "toggle-class",
                "commandName": "dulezite-pasaze",
                "commandOptions": {
                  "toggleClass": "node-selected node-text-orange",
                  "closeBtn": true,
                  "comments": false
                }
              }
            ]
          }
        ]
      }
    },
    {
      "zadani": {
        "hlavni": "Summarize the results of your search.",
        "rozsirujici": ""
      },
      "napoveda": {
        "aktivity": [
          "text"
        ],
        "text": "Write answers to the questions or other comments into the text box (as assigned)."
      },
      "uzivatelskyText": {
        "galerie": [
          {
            "soubor": "why-were-they-displaced/pic-00-1024h.jpg"
          },
          {
            "soubor": "why-were-they-displaced/pic-01-1280w.jpg"
          }
        ],
        "otazky": [
          {
            "id": "text-1",
            "zadani": "Why was the boy displaced?",
            "instrukce": "I think that…",
            "minDelka": 10,
            "maxDelka": 400
          },
          {
            "id": "text-2",
            "zadani": "Why was the young woman displaced?",
            "instrukce": "I think that…",
            "minDelka": 10,
            "maxDelka": 400
          }
        ]
      }
    }
  ]
}
