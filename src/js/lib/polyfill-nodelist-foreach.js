/* eslint-disable no-param-reassign */
/* eslint-disable func-names */
// https://developer.mozilla.org/en-US/docs/Web/API/NodeList/forEach
const polyfillNodelistForeach = () => {
  if (window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = function (callback, thisArg) {
      thisArg = thisArg || window;
      for (let i = 0; i < this.length; i += 1) {
        callback.call(thisArg, this[i], i, this);
      }
    };
  }
};

export default polyfillNodelistForeach;
