/* eslint-disable operator-linebreak */
/* eslint-disable prefer-template */

// https://github.com/ekalinin/typogr.js/blob/master/typogr.js
// (\s+(?:<(?:a|em|span|strong|i|b)[^>]*?>)*?[^\s<>]+(?:<\/(?:a|em|span|strong|i|b)[^>]*?>)*\s+(?:<(?:a|em|span|strong|i|b)[^>]*?>)*?[^\s<>]+(?:<\/(?:a|em|span|strong|i|b)[^>]*?>)*)(?:\s+)([^<>\s]+(?:\s*<\/(?:a|em|span|strong|i|b)[^>]*?>\s*\.*)*?(?:\s*?<\/(?:p|h[1-6]|li|dt|dd)>|$))
const inlineTags = 'a|em|span|strong|i|b';
const fixWidows = (text) => {

  //   (?:
  //     <
  //       (?:${inlineTags})
  //       [^>]*?
  //     >
  //   )*?
  //   [^\\s<>]+
  //   (?:
  //     </
  //       (?:${inlineTags})
  //       [^>]*?
  //     >
  //   )*?
  const word = '(?:<(?:' + inlineTags + ')[^>]*?>)*?[^\\s<>]+(?:</(?:' + inlineTags + ')[^>]*?>)*?';

  const widow = '(' + // matching group 1 START
                '\\s+' + word + // space and a word with a possible bordering tag (match eg.: " word</tag>")
                '\\s+' + word + // space and a word with a possible bordering tag (match eg.: " word</tag>", together (match eg.: " word</tag> word")
                ')' + // matching group 1 END
                '(?:\\s+)' + // one or more space characters
                '(' + // matching group 2 START
                '[^<>\\s]+' + // nontag/nonspace characters
                '(?:\\s*</(?:' + inlineTags + ')[^>]*?>\\s*\\.*)*?' + // one or more inline closing tags, can be surrounded by spaces and followed by a period.
                '(?:\\s*?</(?:p|h[1-6]|li|dt|dd)>|$)' + // allowed closing tags or end of line
                ')'; // matching group 2 END

  return text.replace(new RegExp(widow, 'gi'), '$1&nbsp;$2');
};


const fixOneChar = (text) => {
  // regexr.com/4nfu4
  const oneChar = '(' +
                    '(?:' +
                      '\\s|&nbsp;|<' +
                      '(?:/|' + inlineTags + '){1}' +
                      '[^>]+>[^<>\\s]*' +
                    ')' +
                    '(?:\\s|&nbsp;)*' +
                    '[(|[|{]*' + // (a
                    '[a-ž0-9]{1}' +
                    '[\\.\\,]*?' +
                  ')' +
                  '\\s';

  return text.replace(new RegExp(oneChar, 'gi'), '$1&nbsp;').replace(new RegExp(oneChar, 'gi'), '$1&nbsp;');
};

const typeset = (text) => {

  if (text && text.length > 3) {
    let newText = text;
    newText = fixWidows(newText);
    newText = fixOneChar(newText);

    return newText;
  }

  return text || '';

};

module.exports = typeset;
